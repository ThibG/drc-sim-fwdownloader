import io
import struct

class CRC16Block(io.BytesIO):
    def __init__(self, size, *args, **kwargs):
        self._expected_size = size
        io.BytesIO.__init__(self, *args, **kwargs)


    def commit(self, file):
        self.seek(0)
        contents = self.read()
        assert len(contents) == self._expected_size
        crc16 = self.crc16(contents) # TODO
        file.write(contents)
        file.write(struct.pack('<H', crc16))


    def crc16(self, contents):
        crc = 0xffff
        mask = 0x8408

        for c in contents:
            crc ^= struct.unpack('B', c)[0]
            for j in range(8):
                if crc & 1:
                    crc >>= 1
                    crc ^= mask
                else:
                    crc >>= 1

        return crc


def patch_region(conf, region):
    f = io.BytesIO(conf)

    for base in (0x100, 0x180):
        # TODO
        f.seek(base + 3)
        block = CRC16Block(1)
        block.write(struct.pack('B', region))
        block.commit(f)

    f.seek(0)
    return f.read()
